# Nice test taken from qtcreator.pri
defineTest(minQtVersion) {
    maj = $$1
    min = $$2
    patch = $$3
    isEqual(QT_MAJOR_VERSION, $$maj) {
        isEqual(QT_MINOR_VERSION, $$min) {
            isEqual(QT_PATCH_VERSION, $$patch) {
                return(true)
            }
            greaterThan(QT_PATCH_VERSION, $$patch) {
                return(true)
            }
        }
        greaterThan(QT_MINOR_VERSION, $$min) {
            return(true)
        }
    }
    greaterThan(QT_MAJOR_VERSION, $$maj) {
        return(true)
    }
    return(false)
}

!minQtVersion(5, 4, 0) {
    message("Cannot build Mana Mobile with Qt version $${QT_VERSION}")
    error("Use at least Qt 5.4.0")
}

TEMPLATE = subdirs
CONFIG += ordered

SUBDIRS += src
!tizen:SUBDIRS += example

#OTHER_FILES += \
#    android/AndroidManifest.xml \
#    android/layout/splash.xml \
